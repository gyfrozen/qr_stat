package com.shjywlxx.qrstat;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import net.sf.json.JSONObject;

public class Main {

	private static Map<String, Map<String, Integer>> map = new HashMap<>();
	private static Map<String, Map<String, Integer>> mapEquipe = new HashMap<>();
	
	public static void main(String[] args) throws FileNotFoundException {
//		for (int i = 515; i <= 522; i++) {
//			fetchHosInfo(i+"");			
//		}
//		for (int i = 515; i <= 522; i++) {
//			//stat(i+"");
//			statInOneTravel(i+"");
//		}
//		printStatInOneTravel();
		fetchHosInfo("_0529");
		stat("_0529");
		System.out.println("--搞定！--");
	}
	
	private static void fetchHosInfo(String suffix) throws FileNotFoundException {
		Scanner scanner = new Scanner(new File("equip_stat_day"+suffix+".txt"));
		PrintWriter writer = new PrintWriter(new File("fetch_result"+suffix+".txt"));
		for (int i = 0; scanner.hasNextLine(); i++) {
			System.out.println(i+"...");
			String line = scanner.nextLine();
			line = line.replaceAll("\\\"", "");
			if (line.isEmpty()) continue;
			writer.print(line + "\t");
			JSONObject data = new JSONObject();
	        data.put("equipNo", line);
	        
	        JSONObject obj = doPost("http://jyapi.shjywlxx.com/client/getHosptDeprtLesionByequipNo", data);
	        if (obj != null) {
	        	if (obj.getInt("success") > 0) {
	        		writer.print(obj.getJSONArray("data").getJSONObject(0).getString("hosptName"));
	        		writer.print("\t");
	        		writer.print(obj.getJSONArray("data").getJSONObject(0).getString("departName"));
	        		writer.print("\t");
	        		writer.print(obj.getJSONArray("data").getJSONObject(0).getString("lesionName"));
	        		writer.println();
	        	} else {
	        		writer.println(obj);
	        	}
	        }
		}
		scanner.close();
		writer.close();
	}
	
	private static JSONObject doPost(String url, JSONObject data) {
		try {
            CloseableHttpClient client = null;
            CloseableHttpResponse response = null;
            try {

                HttpPost httpPost = new HttpPost(url);
                httpPost.setHeader(HTTP.CONTENT_TYPE, "application/json");
                httpPost.setEntity(new StringEntity(data.toString(),
                        ContentType.create("text/json", "UTF-8")));

                client = HttpClients.createDefault();
                response = client.execute(httpPost);
                org.apache.http.HttpEntity entity = response.getEntity();
                String result = EntityUtils.toString(entity);
                JSONObject obj = JSONObject.fromObject(result);
                return obj;
            } finally {
                if (response != null) {
                    response.close();
                }
                if (client != null) {
                    client.close();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
		return null;
	}
	
	private static void stat(String suffix) throws FileNotFoundException {
		Scanner scanner = new Scanner(new File("fetch_result"+suffix+".txt"));
		Map<String, Integer> map = new HashMap<>();
		Map<String, Integer> mapEquipe = new HashMap<>();
		Set<String> set = new HashSet<>();
		while(scanner.hasNextLine()){
			String line = scanner.nextLine();
			String[] infoList = line.split("\t");
			if (infoList.length < 2) {
				System.out.println(line);
				continue;
			}
			if (map.containsKey(infoList[1])) {
				map.put(infoList[1], map.get(infoList[1])+1);
				if (!set.contains(infoList[0])) {
					mapEquipe.put(infoList[1], mapEquipe.get(infoList[1])+1);
					set.add(infoList[0]);
				}
			} else {
				map.put(infoList[1], 1);
				mapEquipe.put(infoList[1], 1);
				set.add(infoList[0]);
			}
		}
		scanner.close();
		printStat(map, mapEquipe);
	}
	
	private static void printStat(Map<String, Integer> map, Map<String, Integer> mapEquipe) {
		System.out.print("医院名称");
		System.out.print("\t");
		System.out.print("扫码数量");
		System.out.print("\t");
		System.out.println("平板数量");
		for (Entry<String, Integer> entry : map.entrySet()) {
			System.out.print(entry.getKey());
			System.out.print("\t");
			System.out.print(entry.getValue());
			System.out.print("\t");
			System.out.println(mapEquipe.get(entry.getKey()));
		}
	}
	
	private static void statInOneTravel(String suffix) throws FileNotFoundException {
		Scanner scanner = new Scanner(new File("fetch_result_0"+suffix+".txt"));
		Set<String> set = new HashSet<>();
		while(scanner.hasNextLine()){
			String line = scanner.nextLine();
			String[] infoList = line.split("\t");
			if (infoList.length < 2) {
				System.out.println(line);
				continue;
			}
			if (map.containsKey(infoList[1])) {
				Integer count = map.get(infoList[1]).get(suffix);
				if (count == null) {
					count = 0;
				}
				map.get(infoList[1]).put(suffix, count+1);
				if (!set.contains(infoList[0])) {
					count = mapEquipe.get(infoList[1]).get(suffix);
					if (count == null) {
						count = 0;
					}
					mapEquipe.get(infoList[1]).put(suffix, count+1);
					set.add(infoList[0]);
				}
			} else {
				map.put(infoList[1], new HashMap<>());
				map.get(infoList[1]).put(suffix, 1);
				mapEquipe.put(infoList[1], new HashMap<>());
				mapEquipe.get(infoList[1]).put(suffix, 1);
				set.add(infoList[0]);
			}
		}
		scanner.close();
	}
	
	private static void printStatInOneTravel() {
		System.out.print("医院名称");
		System.out.print("\t");
		System.out.print("扫码数量");
		System.out.print("\t");
		System.out.println("平板数量");
		for (Entry<String, Map<String, Integer>> entry : map.entrySet()) {
			System.out.print(entry.getKey());
			for (int i = 515; i <= 522; i++) {
				if (entry.getValue().containsKey(i+"")) {
					System.out.print("\t");
					System.out.print(entry.getValue().get(i+""));
					System.out.print("\t");
					System.out.print(mapEquipe.get(entry.getKey()).get(i+""));
				} else {
					System.out.print("\t");
					System.out.print(0);
					System.out.print("\t");
					System.out.print(0);
				}
			}
			System.out.println();
		}
	}

}
